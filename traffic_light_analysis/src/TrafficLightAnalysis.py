#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Bool,Float32
from geometry_msgs.msg import Vector3


class TrafficLightAnalysis:
    def __init__(self):
        self.sublightdetected = rospy.Subscriber("traffic_light_detected", Bool, self.callback)
        self.zoneheight = rospy.Publisher("zone_height",Float32, queue_size=50)

    def callback_traffic_size(self, size):
        zone = size.y / 3
        self.zoneheight.publish(zone)



    def callback(self, detectedLight):
        if detectedLight:
            self.subscribeTrafficsize = rospy.Subscriber("traffic_light_size",Vector3,self.callback_traffic_size)


def main():
    analysis = TrafficLightAnalysis()
    rospy.init_node("Analysis", anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting donw")

if __name__ == "__main__":
	main()
