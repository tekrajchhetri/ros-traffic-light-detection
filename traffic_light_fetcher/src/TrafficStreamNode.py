#!/usr/bin/env python

import rospy
from std_msgs.msg import String, Bool
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import darknet_detector
import cv2

class TrafficStreamNode:

    def __init__(self):
        self.bridge = CvBridge()
        self.net = darknet_detector.load_net(b"/home/tekraj/workspace/darknet/cfg/yolov3-tiny.cfg",
                                             b"/home/tekraj/workspace/darknet/yolov3-tiny.weights", 0)
        self.meta = darknet_detector.load_meta(b"/home/tekraj/workspace/darknet/cfg/tekcoco.data")
        self.image_sub = rospy.Subscriber("/cam/image_raw", Image, self.callback)
        self.publishlightdetected = rospy.Publisher("traffic_light_detected",Bool,queue_size=10)
        self.traffic_size = rospy.Publisher("traffic_light_size", Vector3, queue_size=50)

    def callback(self, ros_image):
        rospy.loginfo(ros_image.header)
        vector3_message = Vector3()

        try:
            cv_image = self.bridge.imgmsg_to_cv2(ros_image, "bgr8")
        except CvBridgeError as e:
            rospy.loginfo(e)

        r = darknet_detector.detect(self.net, self.meta, cv_image)

        if len(r) and 'traffic light' in r[0]:  # traffic light from darknet
            x, y, w, h = r[0][2][0], r[0][2][1], r[0][2][2], r[0][2][3]
            vector3_message.x = w
            vector3_message.y = h
            vector3_message.z = 0
            self.publishlightdetected.publish(True)
            self.traffic_size.publish(vector3_message)


        cv2.imshow("IMAGE", cv_image)
        cv2.waitKey(1)
def main():
    camfeed = TrafficStreamNode()
    rospy.init_node("Imageconverter", anonymous=True)


    try:
        rospy.Time.now()
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting donw")
    cv2.destroyAllWindows()

if __name__ == "__main__":
	main()
