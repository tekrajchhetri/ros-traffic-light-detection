# ROS Traffic Light Detection

Reads the video stream and perform the following:

* traffic_light_fetcher node Detect the traffic light using Darknet

    * If detected traffic light publish boolean traffic_light_detected and vector3 traffic_light_size (x= width, y=height, z=0)

* traffic_light_analysis subscribes to fetcher traffic_light_detected

    * if detected then subscribes to traffic_light_size to get dimension

    * once received dimension calculates zone i.e. height/3 (green,red,yellow)

    * publish the result with new topic zone

![](img/topicnodelist.png)
![](img/height.png)
## Getting Started

Follow the instruction below inorder to get run it in your system.
### Prerequisites

You need to install the following inorder to get started.

* Darknet
    * Follow the installation instructions described here https://pjreddie.com/darknet/install/ 
    * While building Darknet, enable OpenCV support
* ROS 
    * Installation instructions - http://wiki.ros.org/ROS/Installation
* usb_cam
    * ROS package for input streaming
* Pretrained model
    * yolov3-tiny.cfg, yolov3-tiny.weights but could be changed to non tiny model for better accuracy.
    * Tiny model is suitable for embedded devices.


### Building
* ROS Node
    * After installing the ROS create the ROS workspace. 
    * Copy the directories i.e. (nodes)  traffic_light_fetcher and traffic_light_analysis to your src directory of rosworkspace
    * From the ros workspace run catkin_make
* Darknet
    * After building from the source, copy tekcoco.data to darknet/cfg directory
    * Replace the Makefile of darknet with the one from **darknet-fix-script** directory
    * Replace image.c and image.h in darknet src directory
    * Build once again
* Path Update:
    * Update your path of yolo cfg,weight,data in TrafficStreamNode by replacing **/home/tekraj/workspace/darknet/**

# Running
* First download the launch files present in launch directory
* roslaunch usbcam.launch and node_launch.launch

## Test
* Tested with ROS Melodic, Darknet from source, Ubuntu 18.
* There is little lag, it is due to the system configuration. If you have GPU support, Darknet could be build with GPU for faster performance.
## Result
* https://youtu.be/nex5c1b_G0Q

 
## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

 

 